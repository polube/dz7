const functions = require('./functions');

describe('first', () => {
    test('isPalindrome', () => {
        expect(functions.isPalindrome('kapAk')).toBeTruthy();
        expect(functions.isPalindrome('cac')).not.toBeFalsy();
        expect(functions.isPalindrome('rorr')).not.toBeNull();
        expect(functions.isPalindrome('Wegew')).toBeTruthy();
        expect(functions.isPalindrome('fof')).not.toBeFalsy();
    })
});
describe('second', () => {
    test('alphabeta', () => {
        expect(functions.alphabeta(5)).not.toBeNull();
        expect(functions.alphabeta(5)).toStrictEqual("1,2,alpha,4,beta");
        expect(functions.alphabeta(2)).toStrictEqual("1,2");
        expect(functions.alphabeta(2)).toBe("1,2");
        expect(functions.alphabeta(3)).toContain("1,2,alpha");
    })
});
describe('thirty', () => {
    test('findVowels', () => {
        expect(functions.findVowels('Heloo')).toBe(3);
        expect(functions.findVowels('Heloo')).not.toBe(2);
        expect(functions.findVowels('Heloao')).not.toBe(2);
        expect(functions.findVowels('fffffff')).toEqual(0);
        expect(functions.findVowels('dddddddd')).toBe(0);
    })
});


describe('fourth', () => {
    test('getter', async () => {


        const data = await functions.getter() 

        expect(data).toStrictEqual("ffdfdf")
        expect(data).toStrictEqual(4324)



        // const publicIp = require('public-ip');

        // async function getIp() {
        //     
        // }

        // functions.getter().then((data) => {
        //     // expect(data).toBe("5.58.101.189");
        //     console.log(data)
        //     expect(data.gt).toEqual(1432424);
        //     expect(data).toBe("1432424");
        //     expect(data).toBeTruthy();

        // });

    });


});
