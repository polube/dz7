// const fetch = require("node-fetch");
import fetch from 'node-fetch';


function isPalindrome(word) {

    let reWord = word.split('').reverse().join('');

    return word.toLowerCase() === reWord.toLowerCase();

    // if (word.toLowerCase() === reWord.toLowerCase()) {
    //     return true;
    // } else {
    //     return false;
    // }

}

function alphabeta(num) {
    let arr = [];

    for (let i = 1; i <= num; i++) {
        if (i % 3 === 0 && i % 5 === 0) {
            arr.push("alphabeta");
        } else if (i % 3 === 0) {
            arr.push("alpha");
        } else if (i % 5 === 0) {
            arr.push("beta");
        } else {
            arr.push(i);
        }

    }

    return arr.join();

}


function findVowels(word) {

    const vowels = ['a', 'o', 'e', 'i', 'u'];

    let res = 0;

    for (let i = 0; i < word.length; i++) {

        for (let a = 0; a < vowels.length; a++) {

            if (word.charAt(i).includes(vowels[a])) {
                res = res + 1;
            }

        }
    }
    return res;

}

async function getter() {

    const publicIp = require('public-ip');

    let random = await fetch("https://httpbin.org/get")
        .then(response => response)

    // console.log(random.origin)

    (async () => {
        console.log(await publicIp.v4());
        // return publicIp.v4();

        if(random.origin === publicIp.v4()){
            return true;
        }
        return false;
    })();

    

    // let out = random.origin;

    // let theIp = await publicIp.v4();
    // console.log(theIp)

    // const publicIp = require('public-ip');

    // async function getIp() {
    //     return await publicIp.v4();
    // }

    // return random;
    // return await publicIp.v4();
    // return random;


    // if(random.origin === publicIp.v4()){
    //     return true;
    // }

    // let theIp = await publicIp.v4();
    // console.log(await publicIp.v4());
    // return (await publicIp.v4());
}









module.exports = {
    isPalindrome,
    alphabeta,
    findVowels,
    getter
};
